import xml.etree.ElementTree as ET
import sys

#prints modules present in bakefile
tree = ET.parse(sys.argv[1])
root = tree.getroot()
dependencies = []
for modules in root:
    for module in modules:
        dependencies.append(module.attrib['name'])
print(" ".join(dependencies))