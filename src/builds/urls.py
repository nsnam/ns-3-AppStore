from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'builds'

urlpatterns = [
    path(
        'history/<str:app>/<str:release>/',
        views.build_history,
        name='build_history'),
]