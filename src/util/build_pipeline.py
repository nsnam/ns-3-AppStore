import requests
from urllib.parse import urlparse, urlunparse
import re
import jenkins
from git.cmd import Git
from apps.models import Build, Release, NsRelease
from appstore.settings.base import jenkins_username, jenkins_password, \
    module_bakefile_token, \
    module_bakefile_pipeline, \
    module_no_bakefile_token, \
    module_no_bakefile_pipeline, \
    fork_bakefile_token, \
    fork_bakefile_pipeline, \
    fork_no_bakefile_token, \
    fork_no_bakefile_pipeline, \
    jenkins_url
from jenkins import JenkinsException
import logging

DIRECT_LINK_CONTENT_TYPES = ['application/gzip', 'application/x-gzip',
                             'application/zip', 'application/x-tar',
                             'application/x-bzip2']
GIT_REPO_NETLOC = ['gitlab.com', 'github.com']
GIT_REPO_REGEX = r'(https:\/\/(github.com|gitlab.com))(\/)([^:\/\s]+)(\/)([^:\/\s]+)(.git)'


def invoke_build(release):
    """Check various fields and calls Jenkins pipeline specific function.

    Args:
        release (apps.models.Release): release for which the build is
                                        called
    Raises:
        ValueError: If there is any problem while creating build object
        jenkins.JenkinsException: If there is any problem
                                while calling build jenkins
    """
    is_module = release.app.app_type == 'M'
    bakefile = release.filename.name
    has_bakefile = bool(bakefile and bakefile.strip())
    has_url = bool(release.url and release.url.strip())
    if is_module:
        if has_bakefile:
            # call jenkins modules+bakefile pipeline
            module_with_bakefile(release)
        else:
            # call jenkins module+without_bakefile pipeline
            if not has_url:
                raise ValueError('None of bakefile and url are present')
            module_without_bakefile(release)
    else:
        if has_bakefile:
            # call jenkins fork+bakefile pipeline
            fork_with_bakefile(release)
        else:
            # call jenkins fork+without_bakefile pipeline
            if not has_url:
                raise ValueError('None of bakefile and url are present')
            fork_without_bakefile(release)


def get_content_type(url):
    """Get content-type from the download url.

    Args:
        url (string): Download url for which the content-type is retrieved

    Returns:
        [string]: Content-type from the url
    """
    headers = requests.head(url, allow_redirects=True).headers
    return headers['Content-Type']


def build_jenkins(pipeline, params, token):
    """Invoke specific Jenkins pipeline build.

    Args:
        pipeline (string): Name of the pipeline
        params (dictionary): Pipeline parameters dictionary
        token (string): Pipeline specific token required to call build

    Raises:
        jenkins.JenkinsException: If any of the statement fails

    Returns:
        int: Returns queue id given to the queued job by Jenkins
    """
    server = jenkins.Jenkins(jenkins_url,
                             username=jenkins_username,
                             password=jenkins_password)
    return server.build_job(pipeline, params, token=token)


def module_with_bakefile(release):
    """Invoke module with bakefile pipeline for a release.
    If successful queue-id is saved in the build object else object is deleted

    Args:
        release (apps.models.Release): Release for which the build is called
    Raises:
        ValueError: If there is any problem while creating build object
        jenkins.JenkinsException: If there is any problem
                                  while calling build jenkins
    """
    try:
        build = create_build_object(release, module_bakefile_pipeline)
        params = {
            'moduleName': release.app.name,
            'moduleVersion': release.version,
            'nsVersion': release.require,
            'id': build.pk
        }
    except BaseException:
        raise ValueError('Some Release fields maybe missing!')
    try:
        queue_id = build_jenkins(module_bakefile_pipeline,
                                 params, module_bakefile_token)
        build.build_id = queue_id
        build.save()
    except jenkins.JenkinsException:
        # Delete the build object as the build cannot be started
        build.delete()
        raise jenkins.JenkinsException


def module_without_bakefile(release):
    """Invoke module without bakefile pipeline for a release.

    Args:
        release (apps.models.Release): Release for which the build is called
    Raises:
        ValueError: If there is any problem while creating build object
        jenkins.JenkinsException: If there is any problem
                                  while calling build jenkins
    """
    # Check content-type from HEAD and decide if method is direct or git
    content_type = get_content_type(release.url)
    if content_type in DIRECT_LINK_CONTENT_TYPES:
        # call jenkins module+without(method: direct) pipeline
        try:
            build = create_build_object(release, module_no_bakefile_pipeline)
            params = {
                'method': 'direct',
                'url': release.url,
                'nsVersion': release.require,
                'id': build.pk
            }
        except BaseException:
            raise ValueError('Some Release fields maybe missing!')
        try:
            queue_id = build_jenkins(module_no_bakefile_pipeline,
                                     params, module_no_bakefile_token)
            build.build_id = queue_id
            build.save()
        except jenkins.JenkinsException:
            # Delete the build object as the build cannot be started
            build.delete()
            raise jenkins.JenkinsException
    else:
        # check if url points to github/gitlab url
        url = release.url
        parsed_url = urlparse(release.url)
        if parsed_url.netloc not in GIT_REPO_NETLOC:
            raise ValueError('Netloc not in predefined list')

        # check with regex https://github.com/username/repo.git
        # if not modify it
        match = re.match(GIT_REPO_REGEX, release.url)
        if match is None:
            # modify the url and check with regex
            modified_path = '/'.join(parsed_url.path.split('/')[:3]) + '.git'
            url = urlunparse(parsed_url._replace(path=modified_path))
            match_again = re.match(GIT_REPO_REGEX, url)
            print(url)
            if match_again is None:
                raise ValueError(
                    'Cannot modify remote git repo link \
                    to match the required format')

        has_tag(url, release.version)
        try:
            build = create_build_object(release, module_no_bakefile_pipeline)
            params = {
                'method': 'git',
                'url': url,
                'nsVersion': release.require,
                'tag': release.version,
                'id': build.pk
            }
        except BaseException:
            raise ValueError('Some Release fields maybe missing!')
        try:
            queue_id = build_jenkins(module_no_bakefile_pipeline,
                                     params, module_no_bakefile_token)
            build.build_id = queue_id
            build.save()
        except jenkins.JenkinsException:
            # Delete the build object as the build cannot be started
            build.delete()
            raise jenkins.JenkinsException


def fork_with_bakefile(release):
    """Invoke fork with bakefile pipeline for a release.

    Args:
        release (apps.models.Release): Release for which the build is called
    Raises:
        ValueError: If there is any problem while creating build object
        jenkins.JenkinsException: If there is any problem
                                  while calling build jenkins
    """
    try:
        build = create_build_object(release, fork_bakefile_pipeline)
        params = {
            'forkName': release.app.name,
            'forkVersion': release.version,
            'nsVersion': release.require,
            'id': build.pk
        }
    except BaseException:
        raise ValueError('Some Release fields maybe missing!')
    try:
        queue_id = build_jenkins(fork_bakefile_pipeline,
                                 params, fork_bakefile_token)
        build.build_id = queue_id
        build.save()
    except jenkins.JenkinsException:
        # Delete the build object as the build cannot be started
        build.delete()
        raise jenkins.JenkinsException


def fork_without_bakefile(release):
    """Invoke fork without bakefile pipeline for a release.

    Args:
        release (apps.models.Release): Release for which the build is called
    Raises:
        ValueError: If there is any problem while creating build object
        jenkins.JenkinsException: If there is any problem
                                  while calling build jenkins
    """
    content_type = get_content_type(release.url)
    if content_type in DIRECT_LINK_CONTENT_TYPES:
        # call jenkins module+without(method: direct) pipeline
        try:
            build = create_build_object(release, fork_no_bakefile_pipeline)
            params = {
                'method': 'direct',
                'url': release.url,
                'id': build.pk
            }
        except BaseException:
            raise ValueError('Some Release fields maybe missing!')

        try:
            queue_id = build_jenkins(fork_no_bakefile_pipeline,
                                     params, fork_no_bakefile_token)
            build.build_id = queue_id
            build.save()
        except jenkins.JenkinsException:
            # Delete the build object as the build cannot be started
            build.delete()
            raise jenkins.JenkinsException
    else:
        # check if url points to github/gitlab url
        url = release.url
        parsed_url = urlparse(release.url)
        if parsed_url.netloc not in GIT_REPO_NETLOC:
            raise ValueError('Netloc not in predefined list')

        # check with regex https://github.com/username/repo.git if not modify
        match = re.match(GIT_REPO_REGEX, release.url)
        if match is None:
            # modify the url and check with regex
            modified_path = '/'.join(parsed_url.path.split('/')[:3]) + '.git'
            url = urlunparse(parsed_url._replace(path=modified_path))
            match_again = re.match(GIT_REPO_REGEX, url)
            if match_again is None:
                raise ValueError(
                    'Cannot modify remote git repo link \
                    to match the required format')

        has_tag(url, release.version)
        try:
            build = create_build_object(release, fork_no_bakefile_pipeline)
            params = {
                'method': 'git',
                'url': url,
                'tag': release.version,
                'id': build.pk
            }
        except BaseException:
            raise ValueError('Some Release fields maybe missing!')
        try:
            queue_id = build_jenkins(fork_no_bakefile_pipeline,
                                     params, fork_no_bakefile_token)
            build.build_id = queue_id
            build.save()
        except jenkins.JenkinsException:
            # Delete the build object as the build cannot be started
            build.delete()
            raise jenkins.JenkinsException


def invoke_dev_builds():
    """Invoke builds on all app releases which use ns-3 as NsRelease.

    Raises:
        ValueError: If there is any problem while creating build object
        jenkins.JenkinsException: If there is any problem
                                while calling build jenkins
    """
    ns_dev = NsRelease.objects.get(name='3-dev')
    releases = Release.objects.filter(require=ns_dev)
    invoked_releases = {}
    for release in releases:
        try:
            invoke_build(release)
            invoked_releases[release.app.name] = release.version
        except (JenkinsException, BaseException):
            print("Can't build : ", release)
    return invoked_releases


def has_tag(url, tag):
    """Check if repo and tag/branch for that repo exist

    Args:
        url (string): Repo url
        tag (string)): Tag/branch for repo

    Raises:
        ValueError: If repo doesn't exist
        ValueError: If tag doesn't exist
    """
    # Check if repo exist or is public using GET request and then check the
    # tag using GitPython
    response = requests.get(url)
    if response.status_code == 404:
        raise ValueError('Repo doesnt exist')
    git = Git()
    tag_info = git.ls_remote("-ht", url, tag)
    if not (tag_info and tag_info.strip()):
        raise ValueError('Tag does not exist for given repo')


def create_build_object(release, pipeline_name):
    """Create build model object for a release

    Args:
        release (apps.models.Release): Release for which the build
                                       model is created
        pipeline_name (string): Pipeline used for building the release

    Returns:
        apps.models.Build: Returns the created build object
    """
    build = Build.objects.create(status=Build.BUILDING,
                                 release=release,
                                 pipeline_name=pipeline_name,
                                 )
    return build


def cancel_build(build):
    """Cancels the Jenkins build.
    There are 3 possible cases :-
    1) Build is already finished/aborted - Nothing is done.
    2) Build is in queue - URL=none and build_status=building.
    3) Build is building - URL!=none and build_status=building - stop_build()

    Args:
        build (apps.models.Build): Build object for which the
                                   build is cancelled

    Raises:
        ValueError: If build is already finished
        jenkens.JenkinsException: If Jenkins REST calls fail
    """
    if build.status != Build.BUILDING:
        # case 1: build is success, failed or aborted
        raise ValueError('Build already finished!')
    server = jenkins.Jenkins(jenkins_url,
                             username=jenkins_username,
                             password=jenkins_password)
    if build.url is None:
        # case 2: build is in queue, so update the build status to aborted
        #         after cancelling
        server.cancel_queue(build.build_id)
        build.status = Build.ABORTED
        build.save()
    else:
        # case 3: build is executing
        server.stop_build(build.pipeline_name, build.build_id)
